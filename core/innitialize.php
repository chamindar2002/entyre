<?php 

session_start();

// Setting up variables
$GLOBALS['config'] = array(
	'mysql'	=> array(
		'host'		=> '127.0.0.1',
		'username'	=> 'root',
		'password'	=> '',
		'db'		=> 'entyre'
	),
	'remember'	=> array(
		'cookie_name'	=> 'hash',
		'cookie_expiry'	=> 604800
	),
	'session'	=> array(
		'session_name'	=> 'user',
		'token_name'	=> 'token'
	),
        'app' => array(
            'name' => 'Antyre Assignment'
        )
);

// Autolaoding
//error_reporting(E_ERROR | E_WARNING | E_PARSE);

spl_autoload_register(function()
{
	
        $path = 'helpers/';

        require_once $path.'user'.'.php';
        require_once $path.'session'.'.php';
        require_once $path.'config'.'.php';
        require_once $path.'db'.'.php';
        require_once $path.'hash'.'.php';
        require_once $path.'cookie'.'.php';
        require_once $path.'input'.'.php';
        require_once $path.'token'.'.php';
        require_once $path.'validation'.'.php';
        
        
        
        
});

require_once 'functions/cleanup.php';

if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name')))
{
	
        $hash 		= Cookie::get(Config::get('remember/cookie_name'));
        
	$hashCheck 	= DB::getInstance()->get('users_session', array('hash', '=', $hash));
       
	if($hashCheck->count())
	{
		$user = new User($hashCheck->first()->user_id);
		$user->login();
                
	}
}