<?php

require_once 'core/innitialize.php';
?>



<html lang="en">
<?php include_once 'inc/_head.php'; ?>
    <body>
        
         <?php include_once 'inc/_nav.php'; ?>

        <div class="container">

            <div class="panel panel-info">
                <div class="panel-heading">Home</div>
                <div class="panel-body">

                    
            <?php 
           
            

            if(!$username = Input::get('user')) {
                    header('Location: index.php');
            } else {
                    $user = new User(Input::get('user'));
                    if(!$user->exists()) {
                            include 'inc/_error.php';
			    exit();
                    } else {
                            $data = $user->data();
                    }
                    ?>
                    <h3><?php echo escape($data->username); ?></h3>
                    <p>Full Name: <?php echo escape($data->name); ?></p>
                    <?php
            }
            ?>
                </div>
            </div>

        </div>

    </body>
</html>
