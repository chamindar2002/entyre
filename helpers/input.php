<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of input
 *
 * @author ACER
 */
class Input {
	public static function exists($type = 'POST')
	{
		switch ($type) {
			case 'POST':
				return (!empty($_POST)) ? true : false;
				break;

			case 'get':
				return (!empty($_GET)) ? true : false;
				break;

			default:
				return false;
				break;
		}
	}

	public static function get($item)
	{
		if(isset($_POST[$item]))
		{
			return $_POST[$item];
		}
		elseif (isset($_GET[$item])) {
			return $_GET[$item];
		}
		return '';
	}
}