<?php
require_once 'core/innitialize.php';

if (Input::exists()) {
    if (Token::check(Input::get('token'))) {
        $validate = new Validation();
        $validation = $validate->check($_POST, array(
            'username' => array(
                'required' => true
            ),
            'password' => array(
                'required' => true
            ),
        ));

        if ($validation->passed()) {
            $user = new User();

            $remember = (Input::get('remember') === 'on') ? true : false;
            $login = $user->login(Input::get('username'), Input::get('password'), $remember);

            if ($login) {
                header('Location: index.php');
            } else {
                echo '<div class="alert alert-danger">Login failed!. Try again.</div>';
            }
        } else {
            foreach ($validation->errors() as $error) {
                echo '<div class="alert alert-danger">';
                    echo $error.'<br />';
                echo '</div>';
                    
                
            }
        }
    }
}
?>

<html lang="en">
    <?php include_once 'inc/_head.php'; ?>
    <body>

        <div class="container">

            <div class="panel panel-info">
                <div class="panel-heading">Login</div>
                <div class="panel-body">

                    <form action="" method="post">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" autocomplete="off" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" autocomplete="off" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="remember">
                                <input type="checkbox" name="remember" id="remember">Remember Me 
                            </label>
                        </div>

                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                        <input type="submit" value="Log In" class="btn btn-primary">
                    </form>
                    
                    <div class="container">
                       
                        <p class="text-muted">Try user: admin, password 123456</p>
                      
                      </div>

                </div>
            </div>

        </div>

    </body>
</html>
