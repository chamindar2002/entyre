<?php
require 'core/innitialize.php';

if(Input::exists()){
	if(Token::check(Input::get('token')) )
	{
		$validate = new Validation();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'users'
			),

			'password' => array(
				'required' => true,
				'min' => 6
			),

			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),

			'name' => array(
				'required' => true,
				'min' => 2,
				'max' => 50
			)
		));

		if($validate->passed()){
			$user = new User();

			$salt = Hash::salt(32);

			try{
				$user->create(array(
					'username'	=> Input::get('username'),
					'password'	=> Hash::make(Input::get('password'), $salt),
					'salt'		=> $salt,
					'name'		=> Input::get('name'),
					'joined'	=> date('Y-m-d H:i:s'),
					'groups'	=> 1
					));

				Session::flash('home', 'Thanks for registering! You can login now.');
				header('Location: index.php');
			}
			catch(Exception $e){
				die($e->getMessage());
			}
		}
		else{
			foreach ($validate->errors() as $error) {
				echo '<div class="alert alert-danger">';
                                    echo $error.'<br />';
                                echo '</div>';
			}
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'inc/_head.php'; ?>
    <body>

        <div class="container">

            <div class="panel panel-info">
                <div class="panel-heading">Home</div>
                <div class="panel-body">
                    <form action="" method="POST">
                            <div class="form-group">
                                    <label for="username">Username:</label>
                                    <input type="text" name="username" class="form-control" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off">
                            </div>

                            <div class="form-group">
                            <label for="password">Password:</label>
                                    <input type="password" name="password" id="password" class="form-control">
                            </div>

                            <div class="form-group">
                            <label for="password_again">Repeat Password:</label>
                                    <input type="password" name="password_again" id="password_again" class="form-control">
                            </div>

                            <div class="form-group">
                            <label for="name">Name:</label>
                                    <input type="text" name="name" class="form-control" id="name" value="<?php echo escape(Input::get('name')); ?>">
                            </div>

                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                            <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>

        </div>

    </body>
</html>
