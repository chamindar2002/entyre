<?php

require_once 'core/innitialize.php';

$user = new User();

if(!$user->isLoggedIn()){
	header('Location: index.php');
}

if(Input::exists())
{
	if(Token::check(Input::get('token'))){

		$validate = new Validation();
		$validation = $validate->check($_POST, array(
			'password_current'	=> array(
				'required'	=> true,
				'min'		=> 6
				),
			'password_new'		=> array(
				'required'	=> true,
				'min'		=> 6
				),
			'password_new_again'	=> array(
				'required'	=> true,
				'min'		=> 6,
				'matches'	=> 'password_new'
				),
		));

		if($validation->passed()){

			if(Hash::make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password){
				echo "Wrong Current Password";
			} else {
				$salt = Hash::salt(32);
				$user->update(array(
					'password'	=> Hash::make(Input::get('password_new'), $salt),
					'salt'		=> $salt
					));

				Session::flash('home', 'Password Changed');
				header('Location: index.php');
			}

		} else {
			foreach ($validation->errors() as $error) {
				echo '<div class="alert alert-danger">';
                                    echo $error.'<br />';
                                echo '</div>';
			}
		}

	}
}
?>



<!DOCTYPE html>
<html lang="en">
<?php include_once 'inc/_head.php'; ?>
    <body>
         <?php include_once 'inc/_nav.php'; ?>

        <div class="container">

            <div class="panel panel-info">
                <div class="panel-heading">Home</div>
                <div class="panel-body">
                    
                    <form action="" method="post">
                            <div class="form-group">
                                    <label for="password">Current Password:</label>
                                    <input class="form-control" type="password" name="password_current" id="password_current">
                            </div>
                            <div class="form-group">
                                    <label for="password">New Password:</label>
                                    <input class="form-control" type="password" name="password_new" id="password_new">
                            </div>

                            <div class="form-group">
                                    <label for="password">Retype password:</label>
                                    <input class="form-control" type="password" name="password_new_again" id="password_new_again">
                            </div>

                            <input type="submit" value="Change Passwrd" class="btn btn-primary">
                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                            </div>
                    </form>
                </div>
            </div>

        </div>

    </body>
</html>
