<?php
require_once 'core/innitialize.php';


$user = new User();
?>


<html lang="en">
<?php include_once 'inc/_head.php'; ?>
    <body>
        <?php include_once 'inc/_nav.php'; ?>

        <div class="container">

            <div class="panel panel-info">
                <div class="panel-heading">Home</div>
                <div class="panel-body">


                    <?php
                    if ($user->isLoggedIn()) {
                        ?>
                                            <p>Hello, <a href="my-profile.php?user=<?php echo escape($user->data()->username); ?>"><?php echo escape($user->data()->username); ?></a>!

                                            <ul>
                                                <li><a href="logout.php">Log Out</a></li>
                                                <li><a href="update.php">Update Information</a></li>
                                                <li><a href="change-password.php">Change Password</a></li>
                                            </ul>
                        <?php
                        if ($user->hasPermission('admin')) {
                            echo '<p>You are the admin</p>';
                        }
                    } else {
                        echo '<p>You need to <a href="login.php">login</a> or <a href="register.php">register</a>';
                    }
                    ?>

                </div>
            </div>

        </div>

    </body>
</html>
