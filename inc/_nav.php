
<?php $user = new User(); ?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><?= Config::get('app/name') ?></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Home</a></li>
      <?php if($user->isLoggedIn()){ ?>
<!--      <li><a href="#">Page 1</a></li>-->
      <li><a href="my-profile.php?user=<?php echo escape($user->data()->username); ?>">My profile</a></li>
      <li><a href="logout.php">Logout : <?php echo escape($user->data()->username); ?></a></li>
      <?php } ?>
    </ul>
  </div>
</nav>

