<?php 

require_once 'core/innitialize.php';

$user = new User();

if(!$user->isLoggedIn())
{
	header('Location: index.php');
}

if(Input::exists())
{
	if(Token::check(Input::get('token'))){

		$validate = new Validation();
		$validation = $validate->check($_POST, array(
			'name'	=> array(
				'required'	=> true,
				'min'		=> 2,
				'max'		=> 50
				)
			));

		if($validation->passed()) {

			try{
				$user->update(array(
					'name'	=> Input::get('name')
				));

				Session::flash('home', 'Details Updated');
				header('Location: index.php');
			} catch(Exception $e){
				die($e->getMessage());
			}

		} else {
			foreach ($validation->errors() as $error) {
				echo $error, '<br>';
			}
		}
	}
}
?>


<html lang="en">
    <?php include_once 'inc/_head.php'; ?>
    <body>
         <?php include_once 'inc/_nav.php'; ?>

        <div class="container">

            <div class="panel panel-info">
                <div class="panel-heading">Update</div>
                <div class="panel-body">

                    <form action="" method="post">
                            <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" name="name" class="form-control" value="<?php echo escape($user->data()->name); ?>">

                                    
                            </div>
                        
                            <input type="submit" value="Update" class="btn btn-primary">
                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                    </form>

                </div>
            </div>

        </div>

    </body>
</html>


