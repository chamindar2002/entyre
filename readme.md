Pre requisites
-----------------------------------------------------------------
PHP 5.6 >
Mysql
Apache

On windows: use XAMPP or WAMP server

Installation
-----------------------------------------------------------------

1) Clone repository to your web root directory.
2) Run db.sql file in your mysql query browser or phpmyadmin
3) Open web browser and go to http://localhost/entyre/ (create vhost if required)
4) Login with credentials username 'admin', password:  '123456'
